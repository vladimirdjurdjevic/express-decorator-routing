import 'mocha';
import * as chai from 'chai';

let should = chai.should();

import { HttpStatusCodes } from '../../../src/http/http-status-codes';
import { HttpActionResult } from '../../../src/http/http-action-result';

describe('HttpActionResult - Unit', () => {
	context('constructor', () => {
		it('should set statusCode and body properties', () => {
			let result = new HttpActionResult(HttpStatusCodes.Ok, { message: 'Ok' });
			result.statusCode.should.equal(HttpStatusCodes.Ok);
			result.body.should.be.ok;
			result.body.message.should.equal('Ok');

			result = new HttpActionResult(HttpStatusCodes.InternalServerError, {});
			result.statusCode.should.equal(HttpStatusCodes.InternalServerError);
			result.body.should.be.ok;
		});
	});

	context('Continue', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Continue({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Continue);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Continue();
			result.statusCode.should.equal(HttpStatusCodes.Continue);
			result.body.should.be.ok;
		});
	});

	context('SwitchingProtocols', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.SwitchingProtocols({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.SwitchingProtocols);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.SwitchingProtocols();
			result.statusCode.should.equal(HttpStatusCodes.SwitchingProtocols);
			result.body.should.be.ok;
		});
	});

	context('Processing', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Processing({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Processing);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Processing();
			result.statusCode.should.equal(HttpStatusCodes.Processing);
			result.body.should.be.ok;
		});
	});

	context('Ok', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Ok({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Ok);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Ok();
			result.statusCode.should.equal(HttpStatusCodes.Ok);
			result.body.should.be.ok;
		});
	});

	context('Created', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Created({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Created);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Created();
			result.statusCode.should.equal(HttpStatusCodes.Created);
			result.body.should.be.ok;
		});
	});

	context('Accepted', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Accepted({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Accepted);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Accepted();
			result.statusCode.should.equal(HttpStatusCodes.Accepted);
			result.body.should.be.ok;
		});
	});

	context('NonAuthoritativeInformation', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NonAuthoritativeInformation({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NonAuthoritativeInformation);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NonAuthoritativeInformation();
			result.statusCode.should.equal(HttpStatusCodes.NonAuthoritativeInformation);
			result.body.should.be.ok;
		});
	});

	context('NoContent', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NoContent({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NoContent);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NoContent();
			result.statusCode.should.equal(HttpStatusCodes.NoContent);
			result.body.should.be.ok;
		});
	});

	context('ResetContent', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.ResetContent({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.ResetContent);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.ResetContent();
			result.statusCode.should.equal(HttpStatusCodes.ResetContent);
			result.body.should.be.ok;
		});
	});

	context('PartialContent', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.PartialContent({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.PartialContent);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.PartialContent();
			result.statusCode.should.equal(HttpStatusCodes.PartialContent);
			result.body.should.be.ok;
		});
	});

	context('MultiStatus', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.MultiStatus({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.MultiStatus);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.MultiStatus();
			result.statusCode.should.equal(HttpStatusCodes.MultiStatus);
			result.body.should.be.ok;
		});
	});

	context('AlreadyReported', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.AlreadyReported({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.AlreadyReported);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.AlreadyReported();
			result.statusCode.should.equal(HttpStatusCodes.AlreadyReported);
			result.body.should.be.ok;
		});
	});

	context('ImUsed', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.ImUsed({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.ImUsed);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.ImUsed();
			result.statusCode.should.equal(HttpStatusCodes.ImUsed);
			result.body.should.be.ok;
		});
	});

	context('MultipleChoices', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.MultipleChoices({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.MultipleChoices);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.MultipleChoices();
			result.statusCode.should.equal(HttpStatusCodes.MultipleChoices);
			result.body.should.be.ok;
		});
	});

	context('MovedPermanently', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.MovedPermanently({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.MovedPermanently);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.MovedPermanently();
			result.statusCode.should.equal(HttpStatusCodes.MovedPermanently);
			result.body.should.be.ok;
		});
	});

	context('Found', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Found({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Found);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Found();
			result.statusCode.should.equal(HttpStatusCodes.Found);
			result.body.should.be.ok;
		});
	});

	context('SeeOther', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.SeeOther({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.SeeOther);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.SeeOther();
			result.statusCode.should.equal(HttpStatusCodes.SeeOther);
			result.body.should.be.ok;
		});
	});

	context('NotModified', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NotModified({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NotModified);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NotModified();
			result.statusCode.should.equal(HttpStatusCodes.NotModified);
			result.body.should.be.ok;
		});
	});

	context('UseProxy', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.UseProxy({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.UseProxy);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.UseProxy();
			result.statusCode.should.equal(HttpStatusCodes.UseProxy);
			result.body.should.be.ok;
		});
	});

	context('TemporaryRedirect', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.TemporaryRedirect({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.TemporaryRedirect);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.TemporaryRedirect();
			result.statusCode.should.equal(HttpStatusCodes.TemporaryRedirect);
			result.body.should.be.ok;
		});
	});

	context('PermanentRedirect', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.PermanentRedirect({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.PermanentRedirect);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.PermanentRedirect();
			result.statusCode.should.equal(HttpStatusCodes.PermanentRedirect);
			result.body.should.be.ok;
		});
	});

	context('BadRequest', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.BadRequest({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.BadRequest);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.BadRequest();
			result.statusCode.should.equal(HttpStatusCodes.BadRequest);
			result.body.should.be.ok;
		});
	});

	context('Unauthorized', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Unauthorized({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Unauthorized);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Unauthorized();
			result.statusCode.should.equal(HttpStatusCodes.Unauthorized);
			result.body.should.be.ok;
		});
	});

	context('PaymentRequired', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.PaymentRequired({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.PaymentRequired);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.PaymentRequired();
			result.statusCode.should.equal(HttpStatusCodes.PaymentRequired);
			result.body.should.be.ok;
		});
	});

	context('Forbidden', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Forbidden({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Forbidden);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Forbidden();
			result.statusCode.should.equal(HttpStatusCodes.Forbidden);
			result.body.should.be.ok;
		});
	});

	context('NotFound', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NotFound({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NotFound);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NotFound();
			result.statusCode.should.equal(HttpStatusCodes.NotFound);
			result.body.should.be.ok;
		});
	});

	context('MethodNotAllowed', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.MethodNotAllowed({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.MethodNotAllowed);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.MethodNotAllowed();
			result.statusCode.should.equal(HttpStatusCodes.MethodNotAllowed);
			result.body.should.be.ok;
		});
	});

	context('NotAcceptable', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NotAcceptable({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NotAcceptable);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NotAcceptable();
			result.statusCode.should.equal(HttpStatusCodes.NotAcceptable);
			result.body.should.be.ok;
		});
	});

	context('ProxyAuthenticationRequired', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.ProxyAuthenticationRequired({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.ProxyAuthenticationRequired);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.ProxyAuthenticationRequired();
			result.statusCode.should.equal(HttpStatusCodes.ProxyAuthenticationRequired);
			result.body.should.be.ok;
		});
	});

	context('RequestTimeout', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.RequestTimeout({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.RequestTimeout);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.RequestTimeout();
			result.statusCode.should.equal(HttpStatusCodes.RequestTimeout);
			result.body.should.be.ok;
		});
	});

	context('Conflict', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Conflict({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Conflict);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Conflict();
			result.statusCode.should.equal(HttpStatusCodes.Conflict);
			result.body.should.be.ok;
		});
	});

	context('Gone', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Gone({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Gone);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Gone();
			result.statusCode.should.equal(HttpStatusCodes.Gone);
			result.body.should.be.ok;
		});
	});

	context('LengthRequired', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.LengthRequired({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.LengthRequired);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.LengthRequired();
			result.statusCode.should.equal(HttpStatusCodes.LengthRequired);
			result.body.should.be.ok;
		});
	});

	context('PreconditionFailed', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.PreconditionFailed({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.PreconditionFailed);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.PreconditionFailed();
			result.statusCode.should.equal(HttpStatusCodes.PreconditionFailed);
			result.body.should.be.ok;
		});
	});

	context('PayloadTooLarge', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.PayloadTooLarge({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.PayloadTooLarge);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.PayloadTooLarge();
			result.statusCode.should.equal(HttpStatusCodes.PayloadTooLarge);
			result.body.should.be.ok;
		});
	});

	context('RequestUriTooLong', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.RequestUriTooLong({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.RequestUriTooLong);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.RequestUriTooLong();
			result.statusCode.should.equal(HttpStatusCodes.RequestUriTooLong);
			result.body.should.be.ok;
		});
	});

	context('UnsupportedMediaType', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.UnsupportedMediaType({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.UnsupportedMediaType);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.UnsupportedMediaType();
			result.statusCode.should.equal(HttpStatusCodes.UnsupportedMediaType);
			result.body.should.be.ok;
		});
	});

	context('RequestedRangeNotSatisfiable', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.RequestedRangeNotSatisfiable({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.RequestedRangeNotSatisfiable);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.RequestedRangeNotSatisfiable();
			result.statusCode.should.equal(HttpStatusCodes.RequestedRangeNotSatisfiable);
			result.body.should.be.ok;
		});
	});

	context('ExpectationFailed', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.ExpectationFailed({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.ExpectationFailed);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.ExpectationFailed();
			result.statusCode.should.equal(HttpStatusCodes.ExpectationFailed);
			result.body.should.be.ok;
		});
	});

	context('MisdirectedRequest', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.MisdirectedRequest({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.MisdirectedRequest);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.MisdirectedRequest();
			result.statusCode.should.equal(HttpStatusCodes.MisdirectedRequest);
			result.body.should.be.ok;
		});
	});

	context('UnprocessableEntity', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.UnprocessableEntity({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.UnprocessableEntity);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.UnprocessableEntity();
			result.statusCode.should.equal(HttpStatusCodes.UnprocessableEntity);
			result.body.should.be.ok;
		});
	});

	context('Locked', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.Locked({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.Locked);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.Locked();
			result.statusCode.should.equal(HttpStatusCodes.Locked);
			result.body.should.be.ok;
		});
	});

	context('FailedDependency', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.FailedDependency({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.FailedDependency);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.FailedDependency();
			result.statusCode.should.equal(HttpStatusCodes.FailedDependency);
			result.body.should.be.ok;
		});
	});

	context('UpgradeRequired', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.UpgradeRequired({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.UpgradeRequired);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.UpgradeRequired();
			result.statusCode.should.equal(HttpStatusCodes.UpgradeRequired);
			result.body.should.be.ok;
		});
	});

	context('PreconditionRequired', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.PreconditionRequired({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.PreconditionRequired);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.PreconditionRequired();
			result.statusCode.should.equal(HttpStatusCodes.PreconditionRequired);
			result.body.should.be.ok;
		});
	});

	context('TooManyRequests', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.TooManyRequests({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.TooManyRequests);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.TooManyRequests();
			result.statusCode.should.equal(HttpStatusCodes.TooManyRequests);
			result.body.should.be.ok;
		});
	});

	context('RequestHeaderFieldsTooLarge', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.RequestHeaderFieldsTooLarge({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.RequestHeaderFieldsTooLarge);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.RequestHeaderFieldsTooLarge();
			result.statusCode.should.equal(HttpStatusCodes.RequestHeaderFieldsTooLarge);
			result.body.should.be.ok;
		});
	});

	context('ConnectionClosedWithoutResponse', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.ConnectionClosedWithoutResponse({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.ConnectionClosedWithoutResponse);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.ConnectionClosedWithoutResponse();
			result.statusCode.should.equal(HttpStatusCodes.ConnectionClosedWithoutResponse);
			result.body.should.be.ok;
		});
	});

	context('UnavailableForLegalReasons', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.UnavailableForLegalReasons({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.UnavailableForLegalReasons);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.UnavailableForLegalReasons();
			result.statusCode.should.equal(HttpStatusCodes.UnavailableForLegalReasons);
			result.body.should.be.ok;
		});
	});

	context('ClientClosedRequest', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.ClientClosedRequest({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.ClientClosedRequest);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.ClientClosedRequest();
			result.statusCode.should.equal(HttpStatusCodes.ClientClosedRequest);
			result.body.should.be.ok;
		});
	});

	context('InternalServerError', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.InternalServerError({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.InternalServerError);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.InternalServerError();
			result.statusCode.should.equal(HttpStatusCodes.InternalServerError);
			result.body.should.be.ok;
		});
	});

		context('NotImplemented', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NotImplemented({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NotImplemented);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NotImplemented();
			result.statusCode.should.equal(HttpStatusCodes.NotImplemented);
			result.body.should.be.ok;
		});
	});

	context('BadGateway', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.BadGateway({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.BadGateway);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.BadGateway();
			result.statusCode.should.equal(HttpStatusCodes.BadGateway);
			result.body.should.be.ok;
		});
	});

	context('ServiceUnavailable', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.ServiceUnavailable({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.ServiceUnavailable);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.ServiceUnavailable();
			result.statusCode.should.equal(HttpStatusCodes.ServiceUnavailable);
			result.body.should.be.ok;
		});
	});

	context('GatewayTimeout', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.GatewayTimeout({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.GatewayTimeout);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.GatewayTimeout();
			result.statusCode.should.equal(HttpStatusCodes.GatewayTimeout);
			result.body.should.be.ok;
		});
	});

	context('HttpVersionNotSupported', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.HttpVersionNotSupported({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.HttpVersionNotSupported);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.HttpVersionNotSupported();
			result.statusCode.should.equal(HttpStatusCodes.HttpVersionNotSupported);
			result.body.should.be.ok;
		});
	});

	context('VariantAlsoNegotiates', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.VariantAlsoNegotiates({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.VariantAlsoNegotiates);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.VariantAlsoNegotiates();
			result.statusCode.should.equal(HttpStatusCodes.VariantAlsoNegotiates);
			result.body.should.be.ok;
		});
	});

	context('InsufficientStorage', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.InsufficientStorage({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.InsufficientStorage);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.InsufficientStorage();
			result.statusCode.should.equal(HttpStatusCodes.InsufficientStorage);
			result.body.should.be.ok;
		});
	});

	context('LoopDetected', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.LoopDetected({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.LoopDetected);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.LoopDetected();
			result.statusCode.should.equal(HttpStatusCodes.LoopDetected);
			result.body.should.be.ok;
		});
	});

	context('NetworkConnectTimeoutError', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NetworkConnectTimeoutError({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NetworkConnectTimeoutError);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NetworkConnectTimeoutError();
			result.statusCode.should.equal(HttpStatusCodes.NetworkConnectTimeoutError);
			result.body.should.be.ok;
		});
	});

	context('NotExtended', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NotExtended({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NotExtended);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NotExtended();
			result.statusCode.should.equal(HttpStatusCodes.NotExtended);
			result.body.should.be.ok;
		});
	});

	context('NetworkAuthenticationRequired', () => {
		it('should return HttpAction result object with appropriate status code and body set', () => {
			let result = HttpActionResult.NetworkAuthenticationRequired({ message: 'Message' });
			result.statusCode.should.equal(HttpStatusCodes.NetworkAuthenticationRequired);
			result.body.should.be.ok;
			result.body.message.should.equal('Message');

			result = HttpActionResult.NetworkAuthenticationRequired();
			result.statusCode.should.equal(HttpStatusCodes.NetworkAuthenticationRequired);
			result.body.should.be.ok;
		});
	});
});