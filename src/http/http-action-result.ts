import { HttpStatusCodes } from './http-status-codes';

export class HttpActionResult {
	public statusCode: HttpStatusCodes;
	public body: any;

	public constructor(statusCode: HttpStatusCodes, body: any) {
		this.statusCode = statusCode;
		this.body = body;
	}

	public static Continue(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Continue, body);
	}

	public static SwitchingProtocols(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.SwitchingProtocols, body);
	}

	public static Processing(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Processing, body);
	}

	public static Ok(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Ok, body);
	}

	public static Created(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Created, body);
	}

	public static Accepted(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Accepted, body);
	}

	public static NonAuthoritativeInformation(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NonAuthoritativeInformation, body);
	}

	public static NoContent(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NoContent, body);
	}

	public static ResetContent(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.ResetContent, body);
	}

	public static PartialContent(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.PartialContent, body);
	}

	public static MultiStatus(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.MultiStatus, body);
	}

	public static AlreadyReported(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.AlreadyReported, body);
	}

	public static ImUsed(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.ImUsed, body);
	}

	public static MultipleChoices(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.MultipleChoices, body);
	}

	public static MovedPermanently(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.MovedPermanently, body);
	}

	public static Found(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Found, body);
	}

	public static SeeOther(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.SeeOther, body);
	}

	public static NotModified(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NotModified, body);
	}

	public static UseProxy(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.UseProxy, body);
	}

	public static TemporaryRedirect(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.TemporaryRedirect, body);
	}

	public static PermanentRedirect(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.PermanentRedirect, body);
	}

	public static BadRequest(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.BadRequest, body);
	}

	public static Unauthorized(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Unauthorized, body);
	}

	public static PaymentRequired(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.PaymentRequired, body);
	}

	public static Forbidden(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Forbidden, body);
	}

	public static NotFound(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NotFound, body);
	}

	public static MethodNotAllowed(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.MethodNotAllowed, body);
	}

	public static NotAcceptable(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NotAcceptable, body);
	}

	public static ProxyAuthenticationRequired(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.ProxyAuthenticationRequired, body);
	}

	public static RequestTimeout(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.RequestTimeout, body);
	}

	public static Conflict(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Conflict, body);
	}

	public static Gone(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Gone, body);
	}

	public static LengthRequired(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.LengthRequired, body);
	}

	public static PreconditionFailed(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.PreconditionFailed, body);
	}

	public static PayloadTooLarge(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.PayloadTooLarge, body);
	}

	public static RequestUriTooLong(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.RequestUriTooLong, body);
	}

	public static UnsupportedMediaType(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.UnsupportedMediaType, body);
	}

	public static RequestedRangeNotSatisfiable(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.RequestedRangeNotSatisfiable, body);
	}

	public static ExpectationFailed(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.ExpectationFailed, body);
	}

	public static MisdirectedRequest(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.MisdirectedRequest, body);
	}

	public static UnprocessableEntity(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.UnprocessableEntity, body);
	}

	public static Locked(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.Locked, body);
	}

	public static FailedDependency(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.FailedDependency, body);
	}

	public static UpgradeRequired(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.UpgradeRequired, body);
	}

	public static PreconditionRequired(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.PreconditionRequired, body);
	}

	public static TooManyRequests(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.TooManyRequests, body);
	}

	public static RequestHeaderFieldsTooLarge(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.RequestHeaderFieldsTooLarge, body);
	}

	public static ConnectionClosedWithoutResponse(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.ConnectionClosedWithoutResponse, body);
	}

	public static UnavailableForLegalReasons(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.UnavailableForLegalReasons, body);
	}

	public static ClientClosedRequest(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.ClientClosedRequest, body);
	}

	public static InternalServerError(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.InternalServerError, body);
	}

	public static NotImplemented(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NotImplemented, body);
	}

	public static BadGateway(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.BadGateway, body);
	}

	public static ServiceUnavailable(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.ServiceUnavailable, body);
	}

	public static GatewayTimeout(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.GatewayTimeout, body);
	}

	public static HttpVersionNotSupported(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.HttpVersionNotSupported, body);
	}

	public static VariantAlsoNegotiates(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.VariantAlsoNegotiates, body);
	}

	public static InsufficientStorage(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.InsufficientStorage, body);
	}

	public static LoopDetected(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.LoopDetected, body);
	}

	public static NotExtended(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NotExtended, body);
	}

	public static NetworkAuthenticationRequired(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NetworkAuthenticationRequired, body);
	}

	public static NetworkConnectTimeoutError(body: any = {}): HttpActionResult {
		return new HttpActionResult(HttpStatusCodes.NetworkConnectTimeoutError, body);
	}
}