import 'reflect-metadata';

export const BaseRouteMetadataSymbol = Symbol('baseRoute');
export const ApiRoutesMetadataSymbol = Symbol('apiRoutes');

const addRoutesToControllerMetadata = function(method, path, argsType) {
	return function(target, key, descriptor) {
		let routes = Reflect.getMetadata(ApiRoutesMetadataSymbol, target);
	    if (!routes) {
	        routes = [];
	        Reflect.defineMetadata(ApiRoutesMetadataSymbol, routes, target);
	    }

		let argMatch = target[key].toString().match(/[^(]*\(([^)]*)\)/);
		let args = [];

		if (argMatch && (argMatch = argMatch[1])) {
			args = argMatch.split(/\W+/);
		}

		routes.push({
			method: method,
			path: path,
			handler: key,
			args: args,
			argsType: argsType
		});

        return descriptor;
	}
}

export const apiController = function(baseRoute = '/') {
	return Reflect.metadata(BaseRouteMetadataSymbol, baseRoute);
};

export const httpGet = function(route = '') {
	return addRoutesToControllerMetadata('get', route, 'form');
}

export const httpPost = function(route = '') {
	return addRoutesToControllerMetadata('post', route, 'form');
}

export const httpPostJson = function(route = '') {
	return addRoutesToControllerMetadata('post', route, 'json');
}

export const httpPut = function(route = '') {
	return addRoutesToControllerMetadata('put', route, 'form');
}

export const httpPutJson = function(route = '') {
	return addRoutesToControllerMetadata('put', route, 'json');
}

export const httpDelete = function(route = '') {
	return addRoutesToControllerMetadata('delete', route, 'form');
}