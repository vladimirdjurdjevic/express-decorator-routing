import { HttpActionResult } from '../http/http-action-result';
import { ApiRoutesMetadataSymbol, BaseRouteMetadataSymbol } from './decorators';
import * as express from 'express';

export class ExpressServer  {
	private port: string | number;
	private express: express.Express;
	private server: any;

	public constructor(port: string | number = 3000) {
		this.port = port;
		this.express = express();
		this.server = null;
	}

	public async start(): Promise<express.Express> {
		await this.stop();
		this.server = await this.express.listen(this.port);

		return this.express;
	}

	public async stop(): Promise<void> {
		if (this.server) {
			await this.server.close();
		}
	}

	public addMiddleware(middleware: any): void {
		this.express.use(middleware);
	}

	public addApiController(controller: any): void {
		let basePath = Reflect.getMetadata(BaseRouteMetadataSymbol, controller.constructor);

		if (!basePath) {
			throw new TypeError('Controller must be class decorated with @apiController decorator.');
		}

		let apiRoutes = Reflect.getMetadata(ApiRoutesMetadataSymbol, controller);

		if (apiRoutes) {
			let router = express.Router();

			for (let route of apiRoutes) {
				router[route.method].apply(router, [route.path, async (req, res, next) => {
					let argValues = [];

					if (route.method === 'post' && route.argsType === 'json') {
						if (route.args.length != 1) {
							throw new Error('httpPostJson route handler must have single parameter (json body) from request.');
						}

						argValues.push(req.body);
					} else if (route.method === 'put' && route.argsType === 'json') {
							if (route.args.length != 2) {
								throw new Error('httpPutJson route handler must have two parameters (Id of target object and new object value as json).');
							}

							argValues.push(req.params[route.args[0] || req.query[route.args[0]]]);
							argValues.push(req.body);
					} else {
						for (let arg of route.args) {
							if (route.method === 'post' && route.argsType === 'form') {
								argValues.push(req.body[arg]);
							} else if (route.method === 'put' && route.argsType === 'form') {
								if (route.args.length < 1) {
									throw new Error('httpPut route handler must have at least one parameter (id of target object).');
								}

								argValues.push(req.params[arg] || req.query[arg] || req.body[arg]);
							} else {
								argValues.push(req.params[arg] || req.query[arg] || req.body[arg]);
							}
						}
					}

					let promise = controller[route.handler].apply(controller, argValues);
					let result;

					if (!(promise instanceof Promise) || !((result = await promise) instanceof HttpActionResult)) {
						throw new TypeError('Api route handler must be async function returning Promise<HttpActionResult>');
					}

					res.status(result.statusCode).send(result.body);
					next();
				}]);
			}

			this.express.use(basePath, router);
		}
	}
}
