Monday, May 8, 2017 - v1.0.0
===========================================
	* Express server wrapper
	* @apiController, @httpGet, @httpPost, @httpPostJson
	* @httpPut, @httpPutJson, @httpDelete