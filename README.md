# express-decorator-routing

Set of decorators for easy controller routing and class wrapper for Express server

## Installation
---

`npm install --save express-decorator-routing`

## Usage
---

Example controller:
```typescript
import { apiController, HttpActionResult, httpGet, httpPostJson, httpDelete } from 'express-decorator-routing';
import { IBookModel, BookModel } from '../../models/book-model';

apiController('/api/v1/books')
export class BookController  {
	@httpGet()
	public async getAllBooks(): Promise<HttpActionResult> {
		try {
			let result = await BookModel.find();
			return HttpActionResult.Ok(result);
		} catch (err) {
			return HttpActionResult.InternalServerError(err);
		}
	}

	@httpDelete('/:id')
	public async deleteBook(id: string): Promise<HttpActionResult> {
		try {
			if (!id) {
				return HttpActionResult.BadRequest({
					message: 'Invalid book id.'
				});
			}

			let result = await BookModel.findByIdAndRemove(id);

			if (!result) {
				return HttpActionResult.NotFound();
			}

			return HttpActionResult.Ok();
		} catch (err) {
			if (err.name === 'CastError' && err.kind === 'ObjectId' && err.path === '_id') {
				return HttpActionResult.BadRequest({
					message: 'Invalid book id.'
				});
			} else {
				return HttpActionResult.InternalServerError(err);
			}
		}
	}

	@httpPostJson()
	public async insertBook(book: IBookModel): Promise<HttpActionResult> {
		try {
			let bookModel = new BookModel(book);
			let result = await bookModel.save();

			return HttpActionResult.Created(result);
		} catch (err) {
			if (err.name === 'ValidationError') {
				return HttpActionResult.BadRequest({
					message: 'Invalid book object',
					errors: err.errors
				});
			}

			return HttpActionResult.InternalServerError(err);
		}
	}
}
```

This example illustrates how you can use `@apiController` decorator to specify base route for your controller,
and `@httpGet`, `@httpDelete`, `@httpPostJson` to add api endpoints. There are `@httpPost`, `@httpPut` and `@httpPutJson`
decorator also.
Important thing is that you can write your methods like `deleteBook(id: string) : Promise<HttpActionResult>`,
and have easily unit testable controllers since you are not passing express `Request`, `Response` and `NextFunction`
objects, and you don't have to mock them.
Alo every method that is api endpoint handler must return `Promise<HttpActionResult>` for mapping between method parameters
and real request and response object to be possible.

Controller UnitTest example:
```typescript
import * as mocha from 'mocha';
import * as sinon from 'sinon';

import { expect } from 'chai';
import { HttpStatusCodes, HttpActionResult } from 'express-decorator-routing';
import { BookController } from './book-controller';
import { BookModel } from '../../models/book-model';

require('sinon-as-promised');

describe('BookController', () => {

    let bookController;
    let bookModelStub;

    beforeEach(() => {
	    bookController = new BookController();
    });

    afterEach(() => {
	    bookModelStub.restore();
    });

    describe('getAllBooks', () => {
	    beforeEach(() => {
		    bookModelStub = sinon.stub(BookModel, 'find');
	    });

	    it ('should return array of IBookModel objects and OK status code', async () => {
		    bookModelStub.resolves([]);

		    let result = await bookController.getAllBooks();

		    expect(result.statusCode).to.equal(HttpStatusCodes.Ok);
		    expect(result.body).to.be.a('array');
		    expect(result.body.length).to.equal(0);

		    bookModelStub.resolves([
			    new BookModel({
				    title: 'Test book 1',
				    author: 'Mean author',
				    year: 2017,
			  	    pages: 1000
			    }),
			    new BookModel({
				    title: 'Test book 2',
				    author: 'Mean author',
				    year: 2017,
				    pages: 800
			    })
		    ]);

		    result = await bookController.getAllBooks();

		    expect(result.statusCode).to.equal(200);
		    expect(result.body).to.be.a('array');
		    expect(result.body.length).to.equal(2);
		    expect(result.body[0].title).to.equal('Test book 1');
		    expect(result.body[1].pages).to.equal(800);
	    });

	    it ('should return InternalServerError status code and error message if something goes wrong', async () => {
		    bookModelStub.rejects('Unknown error');

		    let result = await bookController.getAllBooks();

		    expect(result.statusCode).to.equal(HttpStatusCodes.InternalServerError);
		    expect(result.body.message).to.equals('Unknown error');
	    });
    });

    describe('deleteBook', () => {
	    beforeEach(() => {
		    bookModelStub = sinon.stub(BookModel, 'findByIdAndRemove');
	    });

	    it ('should return BadRequest status code if id has incorrect format', async () => {
		    bookModelStub.rejects({
			    name: 'CastError',
			    kind: 'ObjectId',
			    path: '_id'
		    });

		    let result = await bookController.deleteBook(null);
		    expect(result.statusCode).to.equals(HttpStatusCodes.BadRequest);
		    expect(result.body.message).to.contain('Invalid book id');

		    result = await bookController.deleteBook(undefined);
		    expect(result.statusCode).to.equals(HttpStatusCodes.BadRequest);
		    expect(result.body.message).to.contain('Invalid book id');

		    result = await bookController.deleteBook('aaaa');
		    expect(result.statusCode).to.equals(HttpStatusCodes.BadRequest);
		    expect(result.body.message).to.contain('Invalid book id');
	    });

	    it ('should return NotFound status code if book with given id does not exist', async () => {
		    bookModelStub.resolves(null);

		    let result = await bookController.deleteBook('589b53135540fc1db40db3d7');
		    expect(result.statusCode).to.equals(HttpStatusCodes.NotFound);
		    expect(result.body).to.be.empty;
	    });

	    it ('should return Ok status code if book is successfully removed from database', async () => {
		    bookModelStub.resolves({});

		    let result = await bookController.deleteBook('589b53135540fc1db40db3d7');
		    expect(result.statusCode).to.equals(HttpStatusCodes.Ok);
		    expect(result.body).to.be.empty;
	    });

	    it ('should return InternalServerError status code and error message if something goes wrong', async () => {			        bookModelStub.rejects('Unknown error');

		    let result = await bookController.deleteBook('589b53135540fc1db40db3d7');

		    expect(result.statusCode).to.equal(HttpStatusCodes.InternalServerError);
		    expect(result.body.message).to.equals('Unknown error');
	    });
    });

    describe('insertBook', () => {
	    beforeEach(() => {
		    bookModelStub = sinon.stub(BookModel.prototype, 'save');
	    });

   	    it ('should return BadRequest status code if given object is not valid IBookModel object', async () => {
		    bookModelStub.rejects({
			    name: 'ValidationError',
			    errors: {}
		    });

		    let result = await bookController.insertBook(new BookModel({
			    title: 'Test book 1',
			    author: 'Mean author',
			    year: 'aaa',
			    pages: 1000
		    }));

		    expect(result.statusCode).to.equal(HttpStatusCodes.BadRequest);
		    expect(result.body.message).to.contain('Invalid book object');
	    });

	    it ('should return Created status code and created object if book is successfully added to database', async () => {

		    let newBook = new BookModel({
			    title: 'Test book 1',
			    author: 'Mean author',
			    year: 'aaa',
			    pages: 1000
		    });

		   bookModelStub.resolves(newBook);
		   let result = await bookController.insertBook(newBook);

		   expect(result.statusCode).to.equal(HttpStatusCodes.Created);
		   expect(result.body).to.equal(newBook);
  	    });

	    it ('should return InternalServerError status code and error message if something goes wrong', async () => {
		    bookModelStub.rejects('Unknown error');

		    let result = await bookController.insertBook(new BookModel({
			    title: 'Test book 1',
			    author: 'Mean author',
		 	    year: 2017,
			    pages: 1000
		    }));

		    expect(result.statusCode).to.equal(HttpStatusCodes.InternalServerError);
		    expect(result.body.message).to.equals('Unknown error');
	    });
    });
});
```

After creating controller and unit tests for it, there is last peace of puzzle, to create `ExpressServer`,
and add `BookController` to it.

ExpressServer example
```typescript
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as morgan from 'mongo-morgan';
import * as helmet from 'helmet';

import { ExpressServer } from 'express-decorator-routing';
import { BookController } from '../controllers/book/book-controller';

export class BookApiServer extends ExpressServer {
	public constructor() {
	    super(3000);
	}

	this.addMiddleware(helmet());
	this.addMiddleware(bodyParser.urlencoded({ extended: true }));
	this.addMiddleware(bodyParser.json());
	this.addMiddleware(morgan('dev'));

	this.addApiController(new BookController());
}
```
That's it. In your `main.ts` file you can instantiate `BookApiServer` class and call `start()` method to
start api server.
## Development
---

Want to contribute? Great!
If you are using Visual Studio Code, there is default development setup in `.vscode` folder. There are configured tasks for compile, test and build, and default launch configuration with `main.ts` as entry point, and compile as pre launch task. Be aware that working directory when debugging is not project root, it it `compiled` directory (directory where compiled .js files reside).
There is also `keybindings.json` file with custom bindings which you can copy to your local keybinding.json file (File -> Preferences -> Keyboard Shortcuts). It's just 3 shortcuts, `f4` to run tests, `f6` to run link (build and npm link) task and `ctrl+k, ctrl+d` to format code.
After cloning code, run `npm install` to install dependencies, then run `npm run link` to build library and link library locally for test purposes. 

## Testing
---

To run all tests, run `npm test` command. Different tests can be run separately with `npm run test:unit`, `npm run test:integration` and `npm run test:e2e` commands. 

## Todos
---
 - Reach 100% code coverage
 - Write JSDoc

License
----

MIT
